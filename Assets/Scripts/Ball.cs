﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour 
{
	private void OnCollisionEnter(Collision col)
	{
		// Colidiu com a as paredes de cima ou baixo
		if(col.gameObject.tag == Tags.WALL)
		{
			GameManager.instance.OnBallCollisionWall();
		}

		// Colidiu com o player ou o inimigo
		if(col.gameObject.tag == Tags.PAD)
		{
			GameManager.instance.OnBallCollisionPad();
		}

		// Player fez ponto
		if(col.gameObject.tag == Tags.POINT)
		{
			GameManager.instance.OnCollisionPoint();
		}

		// Inimigo fez ponto
		if(col.gameObject.tag == Tags.MISS)
		{
			GameManager.instance.OnCollisionMiss();
		}
	}
}
