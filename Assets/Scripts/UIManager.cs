﻿using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour 
{
	public Text playerLifes;
	public Text oponentLifes;

	public void Update()
	{
		playerLifes.text = GameManager.instance.playerLifes.ToString();
		oponentLifes.text = GameManager.instance.enemyLifes.ToString();
	}
}
