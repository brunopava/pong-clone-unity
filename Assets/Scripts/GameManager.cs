﻿	using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour 
{
	private static GameManager _instance;
	public static GameManager instance {
		get {
			return _instance;
		}
	}

	public int maxLifes = 3;
	public Vector3 ballDirection = new Vector3(1,1,0);
	public float ballSpeed = 3f;

	public Transform player;
	public Rigidbody ball;

	private int _playerLifes;
	public int playerLifes {
		get {
			return _playerLifes;
		}
	}

	private int _enemyLifes;
	public int enemyLifes {
		get {
			return _enemyLifes;
		}
	}

	private Vector3 _ballInitialPosition;

	public CustomEvent onVictory = new CustomEvent();
	public CustomEvent onLoose = new CustomEvent();

	private void Awake()
	{
		_instance = this;
	}

	private void Start()
	{
		_enemyLifes = maxLifes;
		_playerLifes = maxLifes;
		_ballInitialPosition = ball.transform.position;
		Pause();
	}

	public void Reset()
	{
		_enemyLifes = maxLifes;
		_playerLifes = maxLifes;
		ball.transform.position = _ballInitialPosition;
		Play();
	}
	
	private void Update()
	{
		CheckInput();
		MoveBall();
	}

	public void Pause()
	{
		Time.timeScale = 0;
	}

	public void Play()
	{
		Time.timeScale = 1;
	}

	public void EndGame(bool victory)
	{
		if(victory)
		{
			onVictory.Invoke();
		}else{
			onLoose.Invoke();
		}
	}

	private void CheckInput ()
	{
		Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		player.position = new Vector3(player.position.x,mousePos.y,player.position.z);
	}

	private void MoveBall ()
	{
		ball.velocity = ballDirection*ballSpeed*Time.deltaTime;
	}

	public void OnBallCollisionPad()
	{
		ballDirection = new Vector3(ballDirection.x*-1, ballDirection.y, 0);
	}

	public void OnBallCollisionWall()
	{
		ballDirection = new Vector3(ballDirection.x, ballDirection.y*-1, 0);
	}

	public void OnCollisionPoint()
	{
		_enemyLifes--;
		if(_enemyLifes == 0)
		{
			Pause();
			EndGame(true);
		}

		StartCoroutine(ResetBall());
	}

	public void OnCollisionMiss()
	{
		_playerLifes--;
		if(_playerLifes == 0)
		{
			Pause();
			EndGame(false);
		}

		StartCoroutine(ResetBall());
	}

	private IEnumerator ResetBall ()
	{
		ball.gameObject.SetActive(false);
		yield return new WaitForSeconds(0.5f);
		ball.transform.position = _ballInitialPosition;
		ball.gameObject.SetActive(true);
	}
	
	public void EndGame()
	{
		Reset();
	}
}
