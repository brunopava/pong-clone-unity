﻿using UnityEngine;
using System.Collections;

public class EnemyAI : MonoBehaviour 
{
	private Transform _target;
	public float speed = 0.7f;
	public float responseDelay = 1f;

	private float _delay = 0;

	private void Start()
	{
		_target = GameManager.instance.ball.transform;
	}
	
	public void Update()
	{
		if(ballFacingMe())
		{
			_delay += Time.deltaTime;
			if(_delay>= responseDelay)
			{
				Vector3 position =  new Vector3(transform.localPosition.x,_target.localPosition.y*speed,transform.localPosition.z);
				transform.localPosition =  position;
			}
		}else{
			_delay = 0;
		}
	}

	private bool ballFacingMe()
	{
		if(GameManager.instance.ballDirection.x > 0 )
		{
			return true;
		}
		return false;
	}
}
